# Minecraft Bedrock Docker Standup

```
### 
###  ---------------------------------------------
###   D O C K E R   C O N F I G   S E T T I N G S
###  ---------------------------------------------
###  The values configured here are applied during
###  $ docker-compose up
###  -----------------------------------------------
###  DOCKER-COMPOSE ENVIRONMENT VARIABLES BEGIN HERE
###  -----------------------------------------------
###


PUID=
PGID=
LOG_FILE_NUM=5
LOG_FILE_SIZE=10m
TZ=
EULA=TRUE
GAMEMODE=survival
DIFFICULTY=normal
LEVEL_NAME=
SYNOLOGY_BASE_DOCKER_PATH=/volume1/docker
MINECRAFT_PORT=19132
```
